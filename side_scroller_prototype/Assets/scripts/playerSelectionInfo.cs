﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class playerSelectionInfo : MonoBehaviour {
	int playerCount;
	int maxPlayerCount;
	bool controller1;
	bool controller2;
	bool controller3;
	bool controller4;
	bool keyboard;
	string[] controlOptions = new string[4];
	string[] selectedCharacter = new string[4];
	public string[] selectableCharacters = new string[3];
	public PlayerSelectPanel[] playerSelectPanels = new PlayerSelectPanel[4];



	// Use this for initialization
	void Start () {
		playerCount = 0;
		maxPlayerCount = 4;
		controller1 = false;
		controller2 = false;
		controller3 = false;
		controller4 = false;
		keyboard = false;
		DontDestroyOnLoad (this.gameObject);

		if (playerSelectPanels != null) {
			foreach(PlayerSelectPanel e in playerSelectPanels)
			{
				e.SetActive(false);
				e.SetPlayerSelection(selectableCharacters, 3);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	

		if(Input.GetButton("Accept_Controller1") && controller1 == false && playerCount < maxPlayerCount)
		{
			controller1 = true;
			controlOptions[playerCount] = "Controller1";
			selectedCharacter[playerCount] = playerSelectPanels[playerCount].GetCurrentlySelectedCharacter();
			playerSelectPanels[playerCount].SetActive(true);
			playerSelectPanels[playerCount].setControllerSuffix("Controller1");
			playerCount++;
		}
		if(Input.GetButton("Accept_Controller2") && controller2 == false && playerCount < maxPlayerCount)
		{
			controller2 = true;
			controlOptions[playerCount] = "Controller2";
			selectedCharacter[playerCount] = playerSelectPanels[playerCount].GetCurrentlySelectedCharacter();
			playerSelectPanels[playerCount].SetActive(true);
			playerSelectPanels[playerCount].setControllerSuffix("Controller2");
			playerCount++;
		}
		if(Input.GetButton("Accept_Controller3") && controller3 == false && playerCount < maxPlayerCount)
		{
			controller3 = true;
			controlOptions[playerCount] = "Controller3";
			selectedCharacter[playerCount] = playerSelectPanels[playerCount].GetCurrentlySelectedCharacter();
			playerSelectPanels[playerCount].SetActive(true);
			playerSelectPanels[playerCount].setControllerSuffix("Controller3");
			playerCount++;
		}
		if(Input.GetButton("Accept_Controller4") && controller4 == false && playerCount < maxPlayerCount)
		{
			controller4 = true;
			controlOptions[playerCount] = "Controller4";
			selectedCharacter[playerCount] = playerSelectPanels[playerCount].GetCurrentlySelectedCharacter();
			playerSelectPanels[playerCount].SetActive(true);
			playerSelectPanels[playerCount].setControllerSuffix("Controller4");
			playerCount++;
		}

		if (Input.GetButton ("Accept_Keyboard") && keyboard == false && playerCount < maxPlayerCount) 
		{
			keyboard = true;
			controlOptions[playerCount] = "Keyboard";
			selectedCharacter[playerCount] = playerSelectPanels[playerCount].GetCurrentlySelectedCharacter();
			playerSelectPanels[playerCount].SetActive(true);
			playerSelectPanels[playerCount].setControllerSuffix("Keyboard");
			playerCount++;

		}

		if (Input.GetButton ("start") && playerCount >= 1 && playerCount == numPlayersLockedIn())
		{
			Application.LoadLevel ("test");
		}



	}

	public int GetPlayerCount()
	{
		return playerCount;
	}

	public string GetControllerOption(int playerNum)
	{
		return controlOptions[playerNum];
	}

	public string GetPlayerCharacter(int playerNum)
	{
		return selectedCharacter [playerNum];
	}

	public int numPlayersLockedIn()
	{
		int lockedIn = 0;

		for (int i = 0; i < playerCount; i++) {
			if(playerSelectPanels[i].getIsLockedIn())
				lockedIn++;

		}

		return lockedIn;
	}
}
