﻿using UnityEngine;
using System.Collections;

public class CharacterSheet : MonoBehaviour {
	//this class deals with information about the character's stats and abilities ala a DnD character sheet
	//this is my first contribution to coding -cb
	public string name;
	public int healthPoints = 0;
	public int power = 0;
	public int defense = 0;
	public int spellPower = 0;
	public int criticalHit = 0;
}
