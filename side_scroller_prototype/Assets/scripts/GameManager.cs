﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public GameObject[] enemies = new GameObject[3];
	public GameObject[] playerSpawnPoints = new GameObject[4];
	public PlayerScript player1;
	public PlayerScript player2;
	public Text stats_p1;
	public Text stats_p2;
	public Text[] statBoxes = new Text[4];
	public GameObject[] playerArray = new GameObject[4];
	public GameObject playerToSpawn;
	public GameObject knight;

	playerSelectionInfo playerInfo;

	// Use this for initialization
	void Start () {
		playerSelectionSetup ();



	}
	
	// Update is called once per frame
	void Update () {
		int nullcount = 0;
		foreach (GameObject e in enemies) {
			if(e == null)
				nullcount++;
		}

		if (nullcount == 3)
			Application.LoadLevel ("test_overworld");
	
	}

 void playerSelectionSetup()
	{
		playerInfo = (playerSelectionInfo)FindObjectOfType<playerSelectionInfo> ();
		GameObject spawningCharacter = playerToSpawn;
		int playerCount = playerInfo.GetPlayerCount ();
		for (int i = 0; i < playerCount; i++) {
			if(playerInfo.GetPlayerCharacter(i) == "knight")
			{
				spawningCharacter = knight;
			Debug.Log(playerInfo.GetPlayerCharacter(i));
			}
			GameObject newPlayer = (GameObject)Instantiate(spawningCharacter, playerSpawnPoints[i].transform.position, transform.rotation);
			newPlayer.GetComponent<PlayerScript>().setControllerSuffix(playerInfo.GetControllerOption(i));
			statBoxes[i].text = "health:" + newPlayer.GetComponent<PlayerScript>().getHealth() + "\n" 
				+ "power:" + newPlayer.GetComponent<PlayerScript>().getPower() + "\n"
					+ "spell power:" + newPlayer.GetComponent<PlayerScript>().getSpellPower() + "\n"
					+ "defense:" + newPlayer.GetComponent<PlayerScript>().getDefense() + "\n"
					+ "critical hit:" + newPlayer.GetComponent<PlayerScript>().getCriticalHit();
			
		}
		
		Destroy (playerInfo);
	}

}
