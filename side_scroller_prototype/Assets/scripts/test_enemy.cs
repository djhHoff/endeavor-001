﻿using UnityEngine;
using System.Collections;

public class test_enemy : MonoBehaviour {
	public int health;
	GameObject healthIndicator;

	// Use this for initialization
	void Start () {
		

	
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter2D(Collider2D col)
	{

		GameObject attackingPlayer = null;

		if(col.gameObject.transform.parent != null && col.gameObject.transform.parent.transform.parent != null && col.gameObject.transform.parent.transform.parent.tag == "Player")
		{
			attackingPlayer = col.gameObject.transform.parent.transform.parent.gameObject;
		}

		if(attackingPlayer != null)
		{
		  health = health - attackingPlayer.GetComponent<PlayerScript>().getPower();
			Debug.Log(attackingPlayer.GetComponent<PlayerScript>().getPower());

		  if(health <= 0)
		    Destroy (this.gameObject);
		}

	}
}
