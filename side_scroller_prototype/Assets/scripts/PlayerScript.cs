﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour {

	Rigidbody2D rb = new Rigidbody2D();
	Animator animator = new Animator();
	public float jumpForce;
	public float movementSpeed;
	public float damageForce;
	public int playerNum;
	string name;
	int health;
	int power;
	int defense;
	int spellPower;
	int criticalHit;
	bool isGrounded;
	public bool isFacingRight;
	bool isAttacking;
	bool isDamaged;
	bool canAttack;
	bool canJump;
	string moveAxis;
    string jumpButton;
    string attackButton;
	public string controllerSuffix;
	CharacterSheet characterSheet;
	enum animationStates {right, rightAttack, left, leftAttack};

	// Use this for initialization
	void Start () {
		rb = this.GetComponent<Rigidbody2D> ();
		animator = this.GetComponent<Animator> ();
		characterSheet = this.GetComponent<CharacterSheet> (); 
		isFacingRight = false;
		isAttacking = false;
		isDamaged = false;
		canAttack = true;
		canJump = true;
		name = characterSheet.name;
		health = characterSheet.healthPoints;
		power = characterSheet.power;
		spellPower = characterSheet.spellPower;
		criticalHit = characterSheet.criticalHit;

		moveAxis = "Horizontal_" + controllerSuffix;
		jumpButton = "Jump_" + controllerSuffix;
		attackButton = "Attack_" + controllerSuffix;

	}

	void FixedUpdate () {

		if (Input.GetButton(jumpButton) && isGrounded && canJump) {
			rb.AddForce(Vector2.up * jumpForce);
			isGrounded = false;
			canJump = false;
		}

	

		if (Input.GetAxis(moveAxis) > 0 && !isAttacking && !isDamaged) {
			Vector2 currentVelocity = rb.velocity;
			currentVelocity.x =  movementSpeed;
			rb.velocity = currentVelocity;
			isFacingRight = true;
		}

		if (Input.GetAxis(moveAxis) < 0 && !isAttacking && !isDamaged) {
			Vector2 currentVelocity = rb.velocity;
			currentVelocity.x =  -movementSpeed;
			rb.velocity = currentVelocity;
			isFacingRight = false;
		}

		passiveAbility ();

	
	}

	void Update()
	{
		if (Input.GetButtonUp (jumpButton)) {
			canJump = true;
		}

		if (Input.GetButton (attackButton) && !isAttacking && canAttack) {
			isAttacking = true;
			canAttack = false;
		} 

		if (Input.GetButtonUp (attackButton)) {
			canAttack = true;
		}

		if (health == 0) {
			Application.LoadLevel(Application.loadedLevel);
		}

		if (Input.GetKey(KeyCode.Escape))
			Application.Quit();

		UpdateStates ();
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.tag == "ground") {


		//	if(Physics2D.Raycast(transform.position, -Vector2.up, this.GetComponent<BoxCollider2D>().bounds.extents.y + 0.1f))
		//	{
			isGrounded = true;

			isDamaged = false;
		//	}
		}

		if (col.gameObject.tag == "enemy") {

				rb.AddForce(Vector2.up * jumpForce/2);
				isGrounded = false;
				Vector2 currentVelocity = rb.velocity;
			if(isFacingRight)
			{
				currentVelocity.x =  -damageForce;
			}
			else
			{
				currentVelocity.x = damageForce;

			}
				rb.velocity = currentVelocity;
				isDamaged = true;

			health = health -1;

		}
	}

	void UpdateStates()
	{
		animator.SetBool ("facingRight", this.isFacingRight);
		animator.SetBool ("attacking", this.isAttacking);
	}

public virtual void passiveAbility()
	{
		Debug.Log ("passive activated");
	}
		  

public	void StopAttacking()
	{
		isAttacking = false;
	}


public string getName()
{
		return name;
}
public int getHealth()
{
		return health;
}
public int getPower()
	{
		return power;
	}
public int getSpellPower()
	{
		return spellPower;
	}
public int getDefense()
	{
		return defense;
	}
public void setControllerSuffix(string suffix)
{
		controllerSuffix = suffix;
}
public int getCriticalHit()
	{
		return criticalHit;
	}
}
