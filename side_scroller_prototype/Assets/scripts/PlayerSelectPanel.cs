﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerSelectPanel : MonoBehaviour {

	public Text playerInfoContents;
	public bool isLockedIn;
	string[] playerCharacters;
	string currentlySelectedPlayer;
	string controllerSuffix;
	bool isPanelActive;
	string leftButton;
	string rightButton;
	string acceptButton;
	string moveAxis;
	int playerCharacterIndex;
	int numPlayeableCharacters;
	int timer = 0;
	bool canAccept = false;

	// Use this for initialization
	void Start () {
		playerCharacterIndex = 0;
		isLockedIn = false;

	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
	
		if (isPanelActive && isLockedIn == false) {
			if (Input.GetAxis(moveAxis) < 0 && playerCharacters != null && timer > 10) {
				if (playerCharacterIndex == 0)
				{
					playerCharacterIndex = numPlayeableCharacters -1;
					Debug.Log (numPlayeableCharacters);
				}
				else
					playerCharacterIndex--;

				currentlySelectedPlayer = playerCharacters [playerCharacterIndex];
				SetPlayerInfo (currentlySelectedPlayer);
				timer = 0;
			}

			if (Input.GetAxis(moveAxis) > 0 && playerCharacters != null && timer > 10) {
				if (playerCharacterIndex == numPlayeableCharacters -1)
					playerCharacterIndex = 0;
				else
					playerCharacterIndex++;

				currentlySelectedPlayer = playerCharacters [playerCharacterIndex];
				SetPlayerInfo (currentlySelectedPlayer);
				timer = 0;
			}

			if(Input.GetButton(acceptButton) && canAccept)
			{
				isLockedIn = true;
				SetPlayerInfo(currentlySelectedPlayer + " is locked in");
			}
		}
		timer++;
	}

	public void Update()
	{
		if (Input.GetButtonUp (acceptButton))
			canAccept = true;
	}

	public void SetPlayerInfo(string info)
	{
		playerInfoContents.text = info;
	}

	public void SetActive(bool isActive)
	{
		if (isActive) {
			isPanelActive = true;
			SetPlayerInfo(currentlySelectedPlayer);

		} else {
			isPanelActive = false;
			SetPlayerInfo("press A or Space to select a character");
		}
	}

	public void setControllerSuffix(string suffix)
	{
		controllerSuffix = suffix;
		leftButton = "Left_" + controllerSuffix;
		rightButton = "Right_" + controllerSuffix;
		moveAxis = "Horizontal_" + controllerSuffix;
		acceptButton = "Accept_" + controllerSuffix;
	}

	public bool getIsLockedIn()
	{
		return isLockedIn;
	}

	public void SetPlayerSelection(string[] inPlayerCharacters, int characterCount)
	{
		playerCharacters = inPlayerCharacters;
		currentlySelectedPlayer = playerCharacters [playerCharacterIndex];
		numPlayeableCharacters = characterCount;
	}

	public string GetCurrentlySelectedCharacter()
	{
		return currentlySelectedPlayer;
	}
}
