﻿using UnityEngine;
using System.Collections;

public class Overworld_Player_Script : MonoBehaviour {

	Rigidbody2D rb = new Rigidbody2D();
	public Camera camera;
	public float playerSpeed;
	float directionX;
	float directionY;


	// Use this for initialization
	void Start () {
		rb = this.GetComponent<Rigidbody2D> ();

		float startX = this.transform.position.x;
		float startY = this.transform.position.y;
		float startZ = camera.transform.position.z;
		Vector3 newCameraPos = new Vector3 (startX, startY, startZ);
		camera.transform.position = newCameraPos;
		directionX = 0;
		directionY = 0;
		//Camera.main.transform.position.y = startY;
	
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKey(KeyCode.Escape))
			Application.Quit();
	
	}

	void FixedUpdate () {


		Vector2 directionVector = new Vector2 (directionX, directionY);

		directionVector.Normalize ();

		rb.AddForce (directionVector * playerSpeed);
	

	}

	void OnTriggerEnter2D(Collider2D col)
	{
		Application.LoadLevel ("test");

		
	}
}
